package com.digitallschool.training.spiders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Customer {

    private String firstName;
    private String lastName;
    private String ssn;
    private String gender;
    private LocalDate dob;
    private String phone;
    private String email;
    private String profession;
    private String education;
    private String maritalStatus;
    private String address;
    private String city;
    private String state;
    private Integer pincode;
    private String social;
    private Double income;
    private String hobbies;

    public Customer() {
        super();
    }

    public Customer(String input) {
        String[] values = input.split(",");

        firstName = values[0];
        lastName = values[1];
        ssn = values[2];
        gender = values[3];

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        dob = LocalDate.parse(values[4], formatter);

        phone = values[5];
        email = values[6];
        profession = values[7];
        education = values[8];
        maritalStatus = values[9];
        address = values[10];
        city = values[11];
        state = values[12];
        pincode = Integer.parseInt(values[13]);
        social = values[14];
        income = Double.parseDouble(values[15]);
        hobbies = values[16];
    }

    public int getAge() {

        return Long
                .valueOf(
                        ChronoUnit.YEARS
                                .between(dob, LocalDate.now()))
                .intValue();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

}

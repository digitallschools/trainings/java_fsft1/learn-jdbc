/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class JDBCDemo {

    static BufferedReader input = null;

    static {
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    public static void main(String[] args) throws Exception {
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spider_library", "root", "root");
        Statement st = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);

        ResultSet rs = st.executeQuery("SELECT * FROM authors");

        while (rs.next()) {
            String name = rs.getString(2);
            if (name.equalsIgnoreCase("dietel")) {
                rs.updateString(3, "dietel_dietel@mail.com");

                rs.updateRow();
            }
        }
        
        //rs.insertRow();
        rs.moveToInsertRow();
        rs.updateInt(1, 0);
        rs.updateString(2, "Dennis");
        rs.updateString(3, "dennis@mail.com");
        rs.updateString(4, "Creator of Sea");
        rs.updateString(5, "N/A");
        rs.insertRow();
        
        con.close();
        System.out.println("Finished");
    }

    public static void main1(String[] args) {
        printAuthors();
        System.out.println("###########################################################");
        addAuthor();
        printAuthors();
    }

    public static void addAuthor() {
        try {
            input = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("##### AUTHOR DETAILS #####");
            System.out.print("Name: ");
            String name = input.readLine();
            System.out.print("Email: ");
            String email = input.readLine();
            System.out.print("Profile: ");
            String profile = input.readLine();
            System.out.print("Blog: ");
            String blog = input.readLine();

            try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spider_library", "root", "root");
                    Statement st = con.createStatement();) {
                String str = "INSERT INTO authors VALUES(0, '" + name + "', "
                        + "'" + email + "', '" + profile + "', '" + blog + "')";
                st.executeUpdate(str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printAuthors() {
        //Class.forName("com.mysql.jdbc.Driver");
        //Connection con = null;
        //Statement st = null;
        //ResultSet rs = null;

        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spider_library", "root", "root");
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM authors");) {

            /*ResultSetMetaData rsmd = rs.getMetaData();
        for (int i = 0; i < rsmd.getColumnCount(); i++) {
            System.out.print(rsmd.getColumnName(i + 1) + " # ");
        }
        System.out.println("");*/
            while (rs.next()) {
                /*System.out.println(rs.getInt(1) + " # "
                    + rs.getString(2) + " # "
                    + rs.getString(3) + " # "
                    + rs.getString(4) + " # "
                    + rs.getString(5));*/
                System.out.printf("%3d # %-20s # %-30s # %-35s # %-15s%n",
                        rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

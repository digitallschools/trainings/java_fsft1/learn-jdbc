/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class PSTDemo {

    static BufferedReader input;

    static {
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    public static void main2(String[] args) throws Exception {
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root");
        //Statement st = con.createStatement();
        PreparedStatement pst = con.prepareStatement("INSERT INTO users VALUES(0, ?, ?, ?)");

        List<Customer> customers = new CustomerService().getCustomers();
        System.out.println(customers.size());

        Instant start = Instant.now();
        /*String SQL = "";
        for(Customer c: customers){
            SQL = "INSERT INTO users VALUES(0, '" + c.getFirstName() + "', "
                    + "'" + c.getEmail() + "', '" + c.getLastName() + "')";
            st.executeUpdate(SQL);
        }*/

        for (Customer c : customers) {
            pst.setString(3, c.getLastName());
            pst.setString(1, c.getFirstName());
            pst.setString(2, c.getEmail());

            pst.executeUpdate();
        }

        System.out.println(ChronoUnit.MILLIS.between(start, Instant.now()));

        con.close();
    }

    public static void main(String[] args) throws Exception {
        URL jsonURL = new URL("http://jsonplaceholder.typicode.com/users");

        HttpURLConnection httpCon = (HttpURLConnection) jsonURL.openConnection();
        
        JsonReader reader = Json.createReader(httpCon.getInputStream());
        JsonArray users = reader.readArray();
        reader.close();
        httpCon.disconnect();
                
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root");
        con.setAutoCommit(false);
        
        PreparedStatement pst = con.prepareStatement("INSERT INTO user2 VALUES(0, ?, ?, ?, ?)");
        PreparedStatement pst2 = con.prepareStatement("SELECT user_id FROM user2 where email=?");
        PreparedStatement pst3 = con.prepareStatement("INSERT INTO user2_address VALUES(0, ?, ?, ?, ?, ?, ?, ?)");
        ResultSet rs = null;
        
        for(int i=0; i<users.size(); i++){
            JsonObject user = users.getJsonObject(i);
            
            pst.setString(1, user.getString("name"));
            pst.setString(2, user.getString("username"));
            pst.setString(3, user.getString("email"));
            pst.setString(4, user.getString("website"));
            
            pst.executeUpdate();
            
            pst2.setString(1, user.getString("email"));
            rs = pst2.executeQuery();
            if(rs.next()){
                JsonObject userAddress = user.getJsonObject("address");
                pst3.setInt(1, rs.getInt(1));
                pst3.setString(2, userAddress.getString("street"));
                pst3.setString(3, userAddress.getString("suite"));
                pst3.setString(4, userAddress.getString("city"));
                pst3.setString(5, userAddress.getString("zipcode"));
                pst3.setDouble(6, Double.parseDouble(userAddress.getJsonObject("geo").getString("lat")));
                pst3.setDouble(7, Double.parseDouble(userAddress.getJsonObject("geo").getString("lng")));
                
                pst3.executeUpdate();
            }
        }
        
        System.out.println("Users successfully added to the database.");
        
        con.commit();
        con.setAutoCommit(true);
        rs.close();
        pst2.close();
        pst.close();
        con.close();
        //JsonParser parser = Json.createParser(httpCon.getInputStream());
        //parser.close();
    }
}

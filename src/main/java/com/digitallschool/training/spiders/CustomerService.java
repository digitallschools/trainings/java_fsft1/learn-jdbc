package com.digitallschool.training.spiders;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author DigitallSchool
 */
public class CustomerService {

    private List<Customer> customers;
//\FullStackFull1\java\learn-jdbc\src\main\java

    {
        try {
            String filePath = "C:\\Users\\rkvod\\RKV\\TECHs\\DigitallSchool\\Rupesh\\Projects\\Trainings\\FullStackFull1\\java\\learn-jdbc\\src\\main\\java\\com\\digitallschool\\training\\spiders\\SampleData.csv";
            List<String> inputValues
                    = Files.readAllLines(Paths.get("", filePath));

            customers = inputValues.subList(1, inputValues.size())
                    .stream().map(Customer::new).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Customer> getCustomers() {
        return customers;
    }
}

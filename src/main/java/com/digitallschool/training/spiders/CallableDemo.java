/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Types;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class CallableDemo {

    public static void main(String[] args) throws Exception{
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "root");

        CallableStatement cst = con.prepareCall("{call customer_payment_stats(?, ?, ?, ?)}");
        cst.setInt(1, 10);
        cst.registerOutParameter(2, Types.DOUBLE);
        cst.registerOutParameter(3, Types.DOUBLE);
        cst.registerOutParameter(4, Types.DOUBLE);
        cst.execute();
        
        double average = cst.getDouble(2);
        double max = cst.getDouble(3);
        double min = cst.getDouble(4);
        System.out.println(average + " " + max + " " + min);
        
        con.close();
    }
    
    public static void main1(String[] args) throws Exception {
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "root");

        CallableStatement cst = con.prepareCall("{call payment_total_avg(?)}");
        cst.registerOutParameter(1, Types.DOUBLE);
        cst.execute();
        
        double average = cst.getDouble(1);
        System.out.println(average);
        
        con.close();
    }
}

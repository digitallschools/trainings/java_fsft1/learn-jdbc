/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.jpa.services;

import com.digitallschool.training.spiders.jpa.model.Customer;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class CustomerService {

    static EntityManagerFactory emf;

    static {
        emf = Persistence.createEntityManagerFactory("TEST");
    }

    public List<Customer> findAll() {
        try {
            /*return emf.createEntityManager()
                    .createQuery("SELECT c FROM Customer c", Customer.class)
                    .getResultList();*/

            EntityManager em = emf.createEntityManager();
            TypedQuery<Customer> query = em.createQuery("SELECT c FROM Customer c", Customer.class);
            List<Customer> temp = query.getResultList();
            return temp;
        } catch (Exception ex) {
            return null;
        }
    }

    public Customer findCustomer(Long customerId) {
        try {
            return emf.createEntityManager().find(Customer.class, customerId);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean addCustomer(Customer customer) {
        EntityManager em = null;
        EntityTransaction tx = null;

        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();

            em.persist(customer);

            tx.commit();
            return true;
        } catch (Exception e) {
            if (Objects.nonNull(tx)) {
                tx.rollback();
            }
            return false;
        } finally {
            if (Objects.nonNull(em)) {
                em.close();
            }
        }
    }

    public boolean updateCustomer(Customer customer) {
        EntityManager em = null;
        EntityTransaction tx = null;

        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();

            customer = em.merge(customer);

            tx.commit();
            return true;
        } catch (Exception e) {
            if (Objects.nonNull(tx)) {
                tx.rollback();
            }
            return false;
        } finally {
            if (Objects.nonNull(em)) {
                em.close();
            }
        }
    }

    public boolean deleteCustomer(Long customerId) {
        EntityManager em = null;
        EntityTransaction tx = null;

        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();

            em.remove(em.find(Customer.class, customerId));

            tx.commit();
            return true;
        } catch (Exception e) {
            if (Objects.nonNull(tx)) {
                tx.rollback();
            }
            return false;
        } finally {
            if (Objects.nonNull(em)) {
                em.close();
            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Entity
public class City {

    @Id
    @Column(name = "city_id")
    short cityId;

    @Column(name = "city")
    String name;

    /*@Column(name = "country_id")
    short countryId;*/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    Country country;

    @PostLoad
    public void afterLoading(){
        System.out.println("################City " + name + " ###########");
    }
    
    public short getCityId() {
        return cityId;
    }

    public void setCityId(short cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*public short getCountryId() {
        return countryId;
    }

    public void setCountryId(short countryId) {
        this.countryId = countryId;
    }*/

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "City[" + cityId + ", " + name + ", " + country.countryId + "]";
    }
}

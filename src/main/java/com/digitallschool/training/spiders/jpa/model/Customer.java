/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.jpa.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Entity
@EntityListeners(value = {CustomerEntityListener.class})
@Table(name = "customers")
public class Customer implements Serializable {

    @Id
    @Column(name = "id")
    Long customerId;

    @Column(name = "first_name")
    String firstName;

    @Column(name = "last_name")
    String lastName;

    @Column(name = "amount")
    BigDecimal amount;

    @Version
    @Column(name = "version")
    Integer version;

    public Customer() {
        super();
    }

    public Customer(Long customerId, String firstName, String lastName) {
        setCustomerId(customerId);
        setFirstName(firstName);
        setLastName(lastName);
    }

    public Long getCustomerId() {
        return customerId;
    }

    public Customer setCustomerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Customer setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Customer setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Customer[" + customerId + ", " + firstName + " " + lastName + ", " + amount + "]";
    }
}

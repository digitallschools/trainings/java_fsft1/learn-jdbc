/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.jpa;

import com.digitallschool.training.spiders.jpa.model.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class HibernateMain {

    public static void main(String[] args) {
        SessionFactory factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();

        Country temp = session.get(Country.class, Integer.valueOf(20).shortValue());
        System.out.println(temp);

        session.close();
        factory.close();
    }
}

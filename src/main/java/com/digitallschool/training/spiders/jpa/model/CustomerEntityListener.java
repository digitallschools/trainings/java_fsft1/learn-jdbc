/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.jpa.model;

import javax.persistence.PostLoad;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class CustomerEntityListener {

    //public void afterCustomerLoaded(Object temp) {
    @PostLoad
    public void afterCustomerLoaded(Customer temp) {
        System.out.println("#############Customer: " + temp.firstName);

        //Customer t = (Customer)temp;
        //System.out.println("#############Customer: " + t.firstName);
    }
}

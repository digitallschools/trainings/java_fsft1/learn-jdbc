/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.jpa;

import com.digitallschool.training.spiders.jpa.model.Member;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class LibraryApp {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("spider_library");
        EntityManager em = emf.createEntityManager();

        Member a = em.find(Member.class, 2L);
        //System.out.println("############# " + a.getEmail());

        EntityTransaction tx = em.getTransaction();
        tx.begin();

        //em.remove(a);
        a.setEmail("vijay3@mail.com");

        try {
            Thread.sleep(10000);
        } catch (InterruptedException ie) {

        }

        em.flush();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException ie) {

        }

        System.out.println("The END");
        tx.commit();

        em.close();
        emf.close();
    }
}

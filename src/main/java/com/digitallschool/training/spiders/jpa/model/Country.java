/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.jpa.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Table;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Entity
@Table(name = "country")
@NamedQueries(value
        = {
            @NamedQuery(name = "Country.findAll", query = "SELECT c FROM Country c"),
            @NamedQuery(name = "Country.findById", query = "SELECT c FROM Country c WHERE c.countryId=:countryId"),
            @NamedQuery(name = "Country.findByName",
                    query = "SELECT c FROM Country c WHERE c.name = :countryName")
        })
public class Country implements Serializable {

    @Id
    @Column(name = "country_id")
    short countryId;

    @Column(name = "country")
    String name;

    //@JoinColumn(name = "country_id")
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "country")
    List<City> cities;
    
    @PostLoad
    public void afterLoading(){
        System.out.println("################Country " + name + " ###########");
    }

    public short getCountryId() {
        return countryId;
    }

    public void setCountryId(short countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    @Override
    public String toString() {
        return "Country[" + countryId + ", " + name + "]";
    }
}

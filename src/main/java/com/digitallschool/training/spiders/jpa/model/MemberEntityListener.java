/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.jpa.model;

import javax.persistence.PostLoad;
import javax.persistence.PostRemove;
import javax.persistence.PreRemove;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class MemberEntityListener {

    @PostLoad
    public void memberLoaded(Member m) {
        System.out.println("####### Member: " + m.getName());
    }

    @PreRemove
    public void memberToBeRemoved(Member m) {
        System.out.println("####### Member: " + m.getName() + ", Going to be removed");
    }

    @PostRemove
    public void memberRemoved(Member m) {
        System.out.println("####### Member: " + m.getName() + ", Is removed");
    }
}

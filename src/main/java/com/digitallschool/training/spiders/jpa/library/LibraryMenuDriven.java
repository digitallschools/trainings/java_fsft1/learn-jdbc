/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.jpa.library;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class LibraryMenuDriven {

    static BufferedReader input;
    static EntityManagerFactory emf;

    static {
        input = new BufferedReader(new InputStreamReader(System.in));
        emf = Persistence.createEntityManagerFactory("library");
    }

    public static void main(String[] args) throws Exception {
        String option = "";

        while (true) {
            mainMenu();
            option = input.readLine();

            switch (option) {
                case "1":
                    authorMenuDriven();
                    break;
                case "2":
                    System.out.println("Publishers");
                    break;
                case "3":
                    System.out.println("Books");
                    break;
                case "4":
                    System.out.println("Exiting the app");
                    exit();
                    return;
            }
        }
    }

    public static void authorMenuDriven() throws Exception {
        String option = "";
        while (true) {
            authorMenu();
            option = input.readLine();

            switch (option) {
                case "1":
                    viewAllAuthors();
                    input.readLine();
                    break;
                case "2":
                    addAuthor();
                    break;
                case "3":
                    updateAuthor();
                    break;
                case "4":
                    System.out.println("Exit Author Menu");
                    return;
            }
        }
    }

    public static void exit() {

    }

    public static void mainMenu() {
        System.out.print("\n##### Main Menu #####\n"
                + "1. Authors\n"
                + "2. Publishers\n"
                + "3. Books\n"
                + "4. Exit\n"
                + "Your Option: \n");
    }

    public static void authorMenu() {
        System.out.print("\n##### Author Menu #####\n"
                + "1. View\n"
                + "2. Add\n"
                + "3. Update\n"
                + "4. Exit\n"
                + "Your Option: \n");
    }

    public static void addAuthor() {

    }

    public static void viewAuthor(Long authorId) {

    }

    public static void updateAuthor() {

    }

    public static void viewAllAuthors() {
        List<Author> authors = emf.createEntityManager()
                .createQuery("SELECT a FROM Author a", Author.class)
                .getResultList();

        authors.forEach(a -> {
            System.out.format("| %3d | %-15s | %-20s |%n",
                    a.getAuthorId(), a.getName(), a.getEmail());
        });
    }

}

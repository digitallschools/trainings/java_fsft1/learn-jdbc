/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.jpa;

import com.digitallschool.training.spiders.jpa.model.City;
import com.digitallschool.training.spiders.jpa.model.Country;
import com.digitallschool.training.spiders.jpa.model.Customer;
import com.digitallschool.training.spiders.jpa.services.CustomerService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Metamodel;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class JPAMain {

    static BufferedReader input;

    static {
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    public static void main(String[] args) throws Exception {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TEST");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        Customer c = em.find(Customer.class, 2L, LockModeType.PESSIMISTIC_FORCE_INCREMENT);
        System.out.println(c);

        c.setAmount(c.getAmount().add(new BigDecimal(10)));
        System.out.println(c);
        
        input.readLine();
        

        tx.commit();
        em.close();
        emf.close();
    }

    public static void main9(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TEST");
        EntityManager em = emf.createEntityManager();

        //City c = em.find(City.class, Integer.valueOf(120).shortValue());
        City c = em.createQuery("SELECT c FROM City c WHERE c.cityId=:cityId", City.class)
                .setParameter("cityId", Integer.valueOf(120).shortValue())
                .getSingleResult();
        System.out.println(c);

        System.out.println("################# " + c.getCountry().getName());

        em.close();
        emf.close();
    }

    public static void main8(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TEST");
        EntityManager em = emf.createEntityManager();

        CriteriaBuilder c = em.getCriteriaBuilder();

        CriteriaQuery<Country> query = c.createQuery(Country.class);
        Root<Country> root = query.from(Country.class);
        query.where(
                c.greaterThan(root.get("countryId"), 90),
                c.lessThan(root.get("countryId"), 100),
                c.like(root.get("name"), "T_n%")
        ).orderBy(c.desc(root.get("name")));

        List<Country> countries = em.createQuery(query).getResultList();

        countries.forEach(System.out::println);

        em.close();
        emf.close();
    }

    public static void main7(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            System.out.println("Country Name: ");
            String name = input.readLine();

            emf = Persistence.createEntityManagerFactory("TEST");
            em = emf.createEntityManager();

            Country country = em
                    //.createNamedQuery("Country.findById", Country.class)
                    .createNamedQuery("Country.findByName", Country.class)
                    //.createNamedQuery("Country.findAll", Country.class)
                    .setParameter("countryName", name)
                    //.setParameter("countryId", Integer.valueOf(name).shortValue())
                    .getSingleResult();

            System.out.println(country);
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (PersistenceException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (Objects.nonNull(em)) {
                em.close();
            }
            if (Objects.nonNull(emf)) {
                emf.close();
            }
        }
    }

    public static void main6(String[] args) {
        Customer c = new Customer()
                .setCustomerId(20L)
                .setFirstName("Sam")
                .setLastName("Jackson");
    }

    public static void main5(String[] args) {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            System.out.println("From: ");
            int from = Integer.parseInt(input.readLine());
            System.out.println("To: ");
            int to = Integer.parseInt(input.readLine());

            emf = Persistence.createEntityManagerFactory("TEST");
            em = emf.createEntityManager();

            List<Country> countries = em
                    //.createQuery("SELECT c FROM Country c WHERE c.countryId BETWEEN ?10 AND ?18", Country.class)
                    .createQuery("SELECT c FROM Country c WHERE c.countryId BETWEEN :start AND :end", Country.class)
                    //.setParameter(18, to)
                    //.setParameter(10, from)
                    .setParameter("end", to)
                    .setParameter("start", from)
                    .getResultList();

            countries.forEach(System.out::println);
        } catch (PersistenceException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (Objects.nonNull(em)) {
                em.close();
            }
            if (Objects.nonNull(emf)) {
                emf.close();
            }
        }
    }

    public static void main4(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TEST");
        EntityManager em = emf.createEntityManager();
        Metamodel mm = emf.getMetamodel();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Country> query = builder.createQuery(Country.class);
        Root<Country> countryRoot = query.from(Country.class);
        query.select(countryRoot);
        //query.where(builder.gt(countryRoot.get(mm.entity(Country.class).getSingularAttribute("countryId").getName()), Integer.valueOf(100).shortValue()));
        //query.where(builder.gt(countryRoot.get("countryId"), Integer.valueOf(100).shortValue()));
        List<Country> countries = em.createQuery(query).getResultList();

        countries.forEach(System.out::println);
        emf.close();
    }

    public static void main3(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TEST");
        EntityManager em = emf.createEntityManager();

        /*Country country1 = em.find(Country.class, Integer.valueOf(20).shortValue());

        System.out.println(country1.getCountryId() + ", " + country1.getName());

        List<City> cities = country1.getCities();
        cities.forEach(System.out::println);*/
        City a = em.find(City.class, Integer.valueOf(20).shortValue());
        System.out.println(a.getCityId()
                + " " + a.getName() + ", " + a.getCountry().getName());

        em.close();
        emf.close();
    }

    public static void main2(String[] args) {
        CustomerService service = new CustomerService();

        Customer t = new Customer(2L, "Sundar", "Pitchai");
        service.updateCustomer(t);

    }

    public static void main1(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TEST");
        //EntityManagerFactory emf2 = Persistence.createEntityManagerFactory("TEST");

        /*System.out.println(System.identityHashCode(emf));
        System.out.println(System.identityHashCode(emf2));
        
        System.out.println(emf.isOpen());
        System.out.println(emf2.isOpen());
                
        System.out.println(emf.hashCode());
        System.out.printlnd(emf2.hashCode());*/
        EntityManager em = emf.createEntityManager();

        List<Customer> customers = em.createQuery("SELECT c FROM Customer c", Customer.class)
                .getResultList();

        /*customers.forEach(e -> {
            System.out.println(e.customerId + ", " + e.getFirstName() + ", " + e.getLastName());
        });*/
        for (Customer e : customers) {
            System.out.println(e.getCustomerId() + ", " + e.getFirstName() + ", " + e.getLastName());
        }
        em.close();
        emf.close();
    }
}

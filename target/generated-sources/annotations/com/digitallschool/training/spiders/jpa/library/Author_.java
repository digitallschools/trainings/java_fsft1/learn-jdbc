package com.digitallschool.training.spiders.jpa.library;

import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2019-12-16T18:32:56", comments="EclipseLink-2.7.5.v20191016-rNA")
@StaticMetamodel(Author.class)
public class Author_ { 

    public static volatile SingularAttribute<Author, String> profile;
    public static volatile SingularAttribute<Author, String> name;
    public static volatile SingularAttribute<Author, Long> authorId;
    public static volatile SingularAttribute<Author, String> blog;
    public static volatile SingularAttribute<Author, String> email;

}